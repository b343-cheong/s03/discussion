-- [SECTION] CREATION OF RECORDS / INSERT
-- INSERT INTO table_name (columns_in table) VALUES (values_per_column);

INSERT INTO artists (name) VALUES ("RiverMaya");
INSERT INTO artists (name) VALUES ("Mili");
INSERT INTO artists (name) VALUES ("Anthemics"), ("Soken");

INSERT INTO albums (album_title, date_released, artist_id) VALUES ("ENDWALKER:
FINAL FANTASY XIV Original Soundtrack", "2021-12-7", 4);
INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Arches", "2023-4-28", 3), ("Limbus Company OST", "2023-6-25", 2);

-- [SECTION] Read/Retrieving Records from db
-- SELECT * FROM name_table;
-- artists table
SELECT * FROM artists;

-- albums table
SELECT * FROM artists;


-- We are going to add record in the songs table
INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Flow", 523, 1, "Game Soundtrack");
INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Fly, My Wings", 314, 3, "Game Soundtrack"), ("Bridges", 432, 2, "Game Soundtrack");

--Retrieve Specific Columns

SELECT song_name, length, genre FROM songs;

-- Retrieve Game Soundtrack Genre only

SELECT * FROM songs WHERE genre = "Game Soundtrack";

-- Retrieve FF14 Album Songs only

SELECT * FROM songs WHERE album_id = 1;

-- We can use AND and OR operator for multiple expressions
SELECT * FROM songs WHERE genre = "Game Soundtrack" AND length > 400;

SELECT * FROM songs WHERE album_id = 1 OR length < 350;

-- [SECTION] Updating Record
-- Syntax:
    -- UPDATE table_name SET column = value_to_be WHERE condition;
    UPDATE songs SET genre = "Visual Novel Soundtrack" WHERE song_name = "Bridges";

UPDATE songs SET song_name = "Stickbugged" WHERE length > 200;

UPDATE songs SET id = 10 WHERE song_name = "Fly, My Wings";

INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Between Two Worlds", 456, 3, "Game Soundtrack");

-- [SECTION] Deleting Record
DELETE FROM songs WHERE length = 314;